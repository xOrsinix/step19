package com.example.workmanagerandservices

import retrofit2.http.GET

interface ApiInterface {

    @GET("posts")
    suspend fun getData(): List<PostItem>
}