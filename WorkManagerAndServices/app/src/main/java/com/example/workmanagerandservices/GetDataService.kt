package com.example.workmanagerandservices

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class GetDataService: Service() {

    val TAG = "GetDataService"

    override fun onBind(p0: Intent?): IBinder? = null

    init {
        Log.i(TAG,"Service is running")
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        val retrofitBuilder = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .build()
            .create(ApiInterface::class.java)

        CoroutineScope(Dispatchers.IO).launch {
            for (i in 0..600){
                delay(5000L)
                Log.i(TAG, "${retrofitBuilder.getData()}")
            }
        }

        return START_REDELIVER_INTENT
    }

}