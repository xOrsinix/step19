package com.example.workmanagerandservices

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.work.*
import com.example.workmanagerandservices.databinding.ActivityMainBinding
import kotlinx.coroutines.*
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity() {

    companion object{
        lateinit var retrofitData:List<PostItem>
        lateinit var binding: ActivityMainBinding
    }

    lateinit var adapter: PostItemAdapter

    private var isPostShown = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
        setPeriodicGetDataRequest()
        binding.btGetData.setOnClickListener {
            if (isPostShown==false) {
                isPostShown = true
                for (post in retrofitData) {
                    adapter.addPostItem(post)
                }
            }
            else{
                Toast.makeText(this,"List automatically updates every 15 minutes",Toast.LENGTH_SHORT).show()
            }
        }

        binding.btService.setOnClickListener {
            Intent(this,GetDataService::class.java).also {
                startService(it)
            }
        }
    }

    private fun init(){
        adapter = PostItemAdapter()
        binding.rcView.layoutManager = LinearLayoutManager(applicationContext)
        binding.rcView.adapter = adapter
        retrofitData = listOf()
    }

   private fun setPeriodicGetDataRequest(){
       val workManager = WorkManager.getInstance(applicationContext)
       val getDataRequest = PeriodicWorkRequest.Builder(GetDataWorker::class.java, 16, TimeUnit.MINUTES)
           .build()
       workManager.enqueue(getDataRequest)
       if (!adapter.myPostItemList.isEmpty()){
           adapter.clearAll()
           for (i in retrofitData)
               adapter.addPostItem(i)
       }
    }
}