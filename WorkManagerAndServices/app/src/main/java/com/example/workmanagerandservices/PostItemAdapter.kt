package com.example.workmanagerandservices

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.workmanagerandservices.databinding.PostItemBinding

class PostItemAdapter():RecyclerView.Adapter<PostItemAdapter.PostItemHolder>() {

    val myPostItemList = ArrayList<PostItem>()

    class PostItemHolder(item: View):RecyclerView.ViewHolder(item) {
        val binding = PostItemBinding.bind(item)
        fun bind(postItem: PostItem){
            binding.tvUserId.text = postItem.userId.toString()
            binding.tvPostId.text = postItem.id.toString()
            binding.tvTitle.text = postItem.title
            binding.tvBody.text=postItem.body
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostItemHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.post_item,parent,false)
        return PostItemHolder(view)
    }

    override fun onBindViewHolder(holder:PostItemHolder, position: Int) {
        holder.bind(myPostItemList[position])
    }

    override fun getItemCount(): Int = myPostItemList.size

    fun addPostItem(item: PostItem) {
        myPostItemList.add(item)
        notifyDataSetChanged()
    }

    fun clearAll(){
        myPostItemList.clear()
        notifyDataSetChanged()
    }
}