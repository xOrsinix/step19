package com.example.workmanagerandservices

import android.content.Context
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters
import kotlinx.coroutines.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.Exception

const val BASE_URL = "https://jsonplaceholder.typicode.com/"

class GetDataWorker(context: Context, params: WorkerParameters): Worker(context,params) {
    override fun doWork(): Result {
        try {

            val retrofitBuilder = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
                .create(ApiInterface::class.java)
            CoroutineScope(Dispatchers.IO).launch {
                MainActivity.retrofitData = retrofitBuilder.getData()
                Log.i("GetDataWorker","${MainActivity.retrofitData}")
            }
            return Result.success()
        }
        catch (e: Exception){
            return Result.failure()
        }
    }

}